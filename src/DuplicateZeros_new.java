import java.util.Scanner;

public class DuplicateZeros_new {
    public static int[] arr = { 1, 0, 2, 3, 0, 4, 5, 0 };
    public static int newARR[] = new int[arr.length];

    public static void main(String[] args) {
        duplicateZeros_new();
        printArr();
    }

    private static void printArr() {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    private static void duplicateZeros_new() {

        for (int i = 0; i < arr.length; i++) {
            newARR[i] = arr[i];
        }

        for (int i = 0, j = 0; i < arr.length; i++, j++) {
            if (newARR[j] != 0) {
                arr[i] = newARR[j];
            } else {
                arr[i] = 0;
                i++;
                arr[i] = 0;

            }

        }

    }

    private static void inputIntArrays() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Input size arrays: ");
        int size = kb.nextInt();
        arr = new int[size];

        for (int i = 0; i < arr.length; i++) {
            System.out.print("Input Number: ");
            arr[i] = kb.nextInt();
        }
        System.out.println();
    }
}
