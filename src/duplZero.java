public class duplZero {
    public static void main(String[] args) {
        int arr[] = { 1, 2, 0, 3, 0, 4, 5, 0, 5 };
        duplZero(arr);
        printArray(arr);
    }

    private static void duplZero(int[] arr) {
        int newARR[] = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            newARR[i] = arr[i];
        }
        for (int i = 0, j = 0; i < arr.length; i++, j++) {
            if (newARR[j] != 0) {
                arr[i] = newARR[j];
            } else {
                arr[i] = 0;
                if (i == arr.length - 1) {
                    break;
                }
                i++;
                arr[i] = 0;

            }
        }
    }

    private static void printArray(int[] arr) {
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
