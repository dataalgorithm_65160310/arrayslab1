import java.util.Scanner;

public class DuplicateZeros {
    public static int[] arr;
    public static int arrSize = 0;

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        LoopInputArr();
        System.out.println("Original Array:");
        printArr();

        duplicateZeros();

        System.out.println("Modified Array:");
        printArr();
    }

    private static void LoopInputArr() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Please input N to the next Stage");
        System.out.print("Input Number to Array (Press any non-integer key to stop input): ");
        while (kb.hasNextInt()) {
            int inputNum = kb.nextInt();
            addElement(inputNum);
            System.out.print("input : ");
        }
    }

    private static void addElement(int element) {
        if (arrSize >= arr.length) {
            System.out.println("Array is full. Cannot add more elements.");
            return;
        }
        arr[arrSize++] = element;
    }

    private static void printArr() {
        System.out.print("arr: ");
        for (int i = 0; i < arrSize; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    public static void duplicateZeros() {
        int zeroCount = 0;
        for (int i = 0; i < arrSize; i++) {
            if (arr[i] == 0) {
                zeroCount++;
            }
        }

        int modifiedSize = arrSize + zeroCount;

        if (modifiedSize > arr.length) {
            System.out.println("Not enough space to duplicate zeros.");
            return;
        }

        int i = arrSize - 1;
        int j = modifiedSize - 1;

        while (i >= 0 && j >= 0) {
            if (arr[i] == 0) {
                if (j < arrSize) {
                    arr[j] = 0;
                }
                j--;
                if (j < arrSize) {
                    arr[j] = 0;
                }
            } else {
                if (j < arrSize) {
                    arr[j] = arr[i];
                }
            }
            i--;
            j--;
        }

        arrSize = modifiedSize;
    }
}
