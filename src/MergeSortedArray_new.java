import java.util.Arrays;

public class MergeSortedArray_new {
    public static int[] nums1 = { 1, 2, 3 };
    public static int[] nums2 = { 2, 5, 6 };
    public static int m = nums1.length;
    public static int n = nums2.length;
    public static int[] newArr;

    public static void main(String[] args) {
        swapNums1toNew();
        addOldNumbertoNums1();
        addNums2();
        sortNums1();
        printNums1();
    }

    private static void sortNums1() {
        Arrays.sort(nums1);
    }

    private static void addNums2() {

        for (int i = m, j = 0; i < nums1.length; i++, j++) {
            nums1[i] = nums2[j];
        }
    }

    private static void printNums1() {
        for (int i : nums1) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    private static void addOldNumbertoNums1() {
        nums1 = new int[m + n];
        int i = 0;
        int j = m;
        while (true) {
            --j;
            if (j == -1) {
                break;
            }

            nums1[i++] = newArr[j];
        }

    }

    private static void swapNums1toNew() {
        newArr = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            newArr[i] = nums1[i];
        }
    }

}
