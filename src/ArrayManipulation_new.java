import java.util.Arrays;
import java.util.Scanner;

public class ArrayManipulation_new {
    static int[] numbers = { 5, 8, 3, 2, 7 };
    static String[] names = { "Alice", "Bob", "Charlie", "David" };
    static double[] values = {};

    public static void main(String[] args) {
        printNumbers();
        printNames();
        initialValues();
        sumNumbers();
        maxValues();
        reversedNames();
        sortNumbers();

    }

    private static void sortNumbers() {
        Arrays.sort(numbers);
        System.out.print("Print Sort numbers : ");
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();
    }

    private static void reversedNames() {
        String[] reversedNames = new String[names.length];
        for (int i = 0, j = names.length - 1; i < names.length; i++, j--) {
            reversedNames[i] = names[j];
        }
        System.out.print("Reversed Name : ");
        for (int i = 0; i < reversedNames.length; i++) {
            System.out.print(reversedNames[i] + " ");
        }
        System.out.println();
    }

    private static void maxValues() {
        System.out.print("Maximum of values : ");
        double max = 0;
        for (int i = 0; i < values.length; i++) {
            if (values[i] > max) {
                max = values[i];
            }
        }
        System.out.println(max);
    }

    private static void sumNumbers() {
        System.out.print("Sum of all elemnet in numbers : ");
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        System.out.println(sum);
    }

    private static void initialValues() {
        Scanner kb = new Scanner(System.in);
        values = new double[4];
        double init = 0.1;
        for (int i = 0; i < 4; i++) {
            values[i] = init;
            init += 0.1;
        }
        // for (int i = 0; i < 4; i++) {
        // System.out.print("Input Values : ");
        // kb.nextDouble();
        // }

    }

    private static void printNames() {
        System.out.print("Print names : ");
        for (String i : names) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    private static void printNumbers() {
        System.out.print("Print numbers : ");
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + " ");
        }
        System.out.println();
    }
}